package com.beersdepot.tests;

import com.beersdepot.tests.config.*;
import com.beersdepot.tests.pages.HomePage;
import com.thoughtworks.selenium.SeleneseTestBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import java.util.List;

public class A1_Visit_Site_And_Browse extends TestBase {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			TestUtil.visit("/");

			HomePage homepage = new HomePage(driver);
			// Test if the logo is there
			SeleneseTestBase.assertTrue("Logo is not present", driver
					.findElements(homepage.siteLogo).size() == 1);

			// Test if Register and Login links are there
			TestUtil.assertTextPresent("Register", homepage.register);
			TestUtil.assertTextPresent("Log in", homepage.login);

			// Test if all the menus are there
			TestUtil.assertTextPresent("Home", homepage.homeLink);
			TestUtil.assertTextPresent("Beers", homepage.beersLink);
			TestUtil.assertTextPresent("Contact", homepage.contactLink);
			TestUtil.assertTextPresent("About", homepage.aboutLink);
			TestUtil.assertTextPresent("The Beer Database Project.beta",
					homepage.header);
			TestUtil.assertTextPresent("Discover the World of Beer",
					homepage.subheader);
			TestUtil.assertTextPresent("Add Beer", homepage.addBeer);
			TestUtil.assertTextPresent("Browse by country",
					homepage.browseCountry);
			TestUtil.assertTextPresent("Random Beer", homepage.randomBeer);

			// Assert if Beers page loads up
			TestUtil.click(homepage.beersLink);
			SeleneseTestBase.assertEquals(baseURL + "/beers", driver
					.getCurrentUrl().toString());
			SeleneseTestBase.assertEquals("BY CATEGORY", driver.findElement(By
					.xpath("//div[@class='container']/div/div/div/ul/li")).getText());

			// Assert if Contact page loads up
			TestUtil.click(homepage.contactLink);
			SeleneseTestBase.assertEquals("Name:", driver.findElement(
					By.xpath("//div[@id='contact']/form/label[1]")).getText());
			
			// Assert if About page loads up
			TestUtil.click(homepage.aboutLink);
			SeleneseTestBase.assertEquals("About", driver.findElement(
					By.xpath("//div[@class='container']/h1")).getText());

			driver.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
