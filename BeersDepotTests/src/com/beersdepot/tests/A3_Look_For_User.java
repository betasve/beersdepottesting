package com.beersdepot.tests;

import org.openqa.selenium.By;
import com.beersdepot.tests.config.*;
import java.util.List;
import org.openqa.selenium.WebElement;
import com.beersdepot.tests.pages.HomePage;
import com.beersdepot.tests.pages.SearchPage;
import com.thoughtworks.selenium.SeleneseTestBase;

public class A3_Look_For_User extends TestBase {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			TestUtil.visit("/");

			HomePage homepage = new HomePage(driver);

			homepage.search("Lalev");

			SearchPage search = new SearchPage(driver);
			
			List<WebElement> rows = null;
			
			// Assert tabs 
			// Beers
			TestUtil.assertTextPresent("Beers (0)", search.tabBeers);
			rows = search.getTabResults(search.beers);
			SeleneseTestBase.assertFalse("Beers found", TestUtil.findRow("", rows));
			
			// Breweries
			TestUtil.assertTextPresent("Breweries (0)", search.tabBreweries);
			TestUtil.click(search.tabBreweries);
			rows = search.getTabResults(search.breweries);
			SeleneseTestBase.assertFalse("Breweries found", TestUtil.findRow("", rows));
			
			// Styles
			TestUtil.assertTextPresent("Styles (0)", search.tabStyles);
			TestUtil.click(search.tabStyles);
			rows = search.getTabResults(search.styles);
			SeleneseTestBase.assertFalse("Styles found", TestUtil.findRow("", rows));
			
			// Categories
			TestUtil.assertTextPresent("Categories (0)", search.tabCategories);
			TestUtil.click(search.tabCategories);
			rows = search.getTabResults(search.categories);
			SeleneseTestBase.assertFalse("Categories found", TestUtil.findRow("", rows));
			
			// Countries
			TestUtil.assertTextPresent("Countries (0)", search.tabCountries);
			TestUtil.click(search.tabCountries);
			rows = search.getTabResults(search.countries);
			SeleneseTestBase.assertFalse("Countries found", TestUtil.findRow("", rows));
			
			// Comments
			TestUtil.assertTextPresent("Comments (0)", search.tabComments);
			TestUtil.click(search.tabComments);
			rows = search.getTabResults(search.comments);
			SeleneseTestBase.assertFalse("Comments found", TestUtil.findRow("", rows));
			
			// Users
			TestUtil.assertTextPresent("Users (1)", search.tabUsers);
			TestUtil.click(search.tabUsers);
			rows = search.getTabResults(search.users);
			SeleneseTestBase.assertTrue("Users not found", TestUtil.findRow("Georgi Lalev", rows));
			
			driver.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
