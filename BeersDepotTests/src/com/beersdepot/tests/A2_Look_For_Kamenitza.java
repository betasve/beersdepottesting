package com.beersdepot.tests;

import java.util.List;
import com.beersdepot.tests.config.*;
import org.openqa.selenium.WebElement;
import com.beersdepot.tests.pages.HomePage;
import com.beersdepot.tests.pages.SearchPage;
import com.thoughtworks.selenium.SeleneseTestBase;



public class A2_Look_For_Kamenitza extends TestBase {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TestUtil.visit("/");
		HomePage homepage = new HomePage(driver);
		
		homepage.search("Kamenitza");
		
		// Move to search page
		SearchPage search = new SearchPage(driver);
		TestUtil.assertTextPresent("Beers (4)", search.tabBeers);
		
		// Get list of beers in this tab
		List<WebElement> rows = search.getTabResults(search.beers);
		
		// Look for Kamenitza Svetlo in the results
		SeleneseTestBase.assertTrue("Kamenitza Svetlo not found", TestUtil.findRow("Kamenitza Svetlo", rows));
		
		// Look for Kamenitza Tamno in the results
		SeleneseTestBase.assertTrue("Kamenitza Tamno not found", TestUtil.findRow("Kamenitza Tamno", rows));

		// Look for Kamenitza Fresh Lemon in the results
		SeleneseTestBase.assertTrue("Kamenitza Fresh Lemon not found", TestUtil.findRow("Kamenitza Fresh Lemon", rows));
		
		// Look for Astika in the results
		SeleneseTestBase.assertTrue("Astika not found", TestUtil.findRow("Astika", rows));
		
		
		// Assert Breweries tab
		TestUtil.assertTextPresent("Breweries (1)", search.tabBreweries);
		TestUtil.click(search.tabBreweries);
		
		// Look for Kamenitza AD brewery
		rows = search.getTabResults(search.breweries);
		SeleneseTestBase.assertTrue("Kamenitza Ad not found", TestUtil.findRow("Kamenitza AD", rows));
		
		// Assert Styles tab
		TestUtil.assertTextPresent("Styles (0)", search.tabStyles);
		TestUtil.click(search.tabStyles);
		rows = search.getTabResults(search.styles);
		SeleneseTestBase.assertFalse("Styles found", TestUtil.findRow("", rows));
		
		// Assert Categories
		TestUtil.assertTextPresent("Categories (0)", search.tabCategories);
		TestUtil.click(search.tabCategories);
		rows = search.getTabResults(search.categories);
		SeleneseTestBase.assertFalse("Categories found", TestUtil.findRow("", rows));
		
		// Assert Countries
		TestUtil.assertTextPresent("Countries (0)", search.tabCountries);
		TestUtil.click(search.tabCountries);
		rows = search.getTabResults(search.countries);
		SeleneseTestBase.assertFalse("Countries found", TestUtil.findRow("", rows));
		
		// Assert Comments
		TestUtil.assertTextPresent("Comments (1)", search.tabComments);
		
		// Look for the right comment
		TestUtil.click(search.tabComments);
		rows = search.getTabResults(search.comments);
		SeleneseTestBase.assertTrue("Comment not found", TestUtil.findRow("Kamenitza Rules!!!", rows));
		
		// Assert Users
		TestUtil.assertTextPresent("Users (0)", search.tabUsers);
		TestUtil.click(search.tabUsers);
		rows = search.getTabResults(search.users);
		SeleneseTestBase.assertFalse("Users found", TestUtil.findRow("", rows));
		
		driver.close();
	}

}
