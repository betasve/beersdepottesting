package com.beersdepot.tests;

import java.util.List;
import com.beersdepot.tests.config.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.beersdepot.tests.pages.HomePage;
import com.beersdepot.tests.pages.SearchPage;
import com.thoughtworks.selenium.SeleneseTestBase;
import com.beersdepot.tests.pages.UserProfilePage;

public class A4_Test_User_Profile extends TestBase {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			TestUtil.visit("/");

			HomePage homepage = new HomePage(driver);
			homepage.search("Lalev");

			SearchPage search = new SearchPage(driver);

			TestUtil.click(search.tabUsers);
			List<WebElement> rows = search.getTabResults(search.users);
			SeleneseTestBase.assertTrue("Users not found", TestUtil.findRow(
					"Georgi Lalev", rows));
			// By userToClickOn = driver.findElement(By.linkText("Georgi
			// Lalev"));
			TestUtil.click(By.linkText("Georgi Lalev"));

			UserProfilePage profile = new UserProfilePage(driver);
			
			// Assert profile image is there
			SeleneseTestBase.assertTrue(driver.findElement(profile.profilePicture).isDisplayed());
			
			// Assert following etc images are there and they have text
			SeleneseTestBase.assertTrue("Following image is not there", driver.findElement(profile.followingIcon).isDisplayed());
			SeleneseTestBase.assertFalse("Following number is not there", driver.findElement(profile.followingNum).getText().isEmpty());
			SeleneseTestBase.assertTrue("Opened beers image is not there", driver.findElement(profile.openedBeers).isDisplayed());
			SeleneseTestBase.assertFalse("Opened beers number is not there", driver.findElement(profile.openedBeersNum).getText().isEmpty());
			SeleneseTestBase.assertTrue("Added beers image is not there", driver.findElement(profile.addedBeers).isDisplayed());
			SeleneseTestBase.assertFalse("Added beers number is not there", driver.findElement(profile.addedBeersNum).getText().isEmpty());
			SeleneseTestBase.assertTrue("Favorite beers image is not there", driver.findElement(profile.favBeers).isDisplayed());
			SeleneseTestBase.assertFalse("Favorite beers number is not there", driver.findElement(profile.favBeersNum).getTagName().isEmpty());
			SeleneseTestBase.assertTrue("Followers image is not there", driver.findElement(profile.followers).isDisplayed());
			SeleneseTestBase.assertFalse("Followers num is not there", driver.findElement(profile.followersNum).getText().isEmpty());
			
			// Assert profile info is displayed
			System.out.println(driver.findElement(profile.profileInfo).getText());
			SeleneseTestBase.assertTrue("Profile info is not displayed", driver.findElement(profile.profileInfo).getText().equals("Georgi Lalev 27 years old male from Bulgaria "));
			SeleneseTestBase.assertTrue("Profile flag is not displayed", driver.findElement(profile.profileInfoFlag).isDisplayed());
			
			TestUtil.click(profile.beersFlowTab);
			SeleneseTestBase.assertTrue("There's nothing in beers tab", search.getTabResults(profile.beersFlowContent).size() > 0);
			
			TestUtil.click(profile.statisticsTab);
			SeleneseTestBase.assertTrue("There's nothing in the statistics tab", search.getTabResults(profile.statsContent).size() > 0);
			
			TestUtil.click(profile.addedBeersTab);
			SeleneseTestBase.assertTrue("There's nothing in added beers tab", search.getTabResults(profile.addedBeersContent).size() > 0);
			
			TestUtil.click(profile.favBeersTab);
			SeleneseTestBase.assertTrue("There's nothing in fav beers tab", search.getTabResults(profile.favBeersContent).size() > 0);
			
			driver.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
