package com.beersdepot.tests.pages;

import org.openqa.selenium.By;
import com.beersdepot.tests.config.*;
import org.openqa.selenium.WebDriver;

public class MainMenu extends TestBase {
	
	public MainMenu(WebDriver driver){
		if(!"Beers Depot".equals(driver.findElement(siteLogo))){
			throw new IllegalStateException("This is not the main menu");
		}
	}

	public By siteLogo = By.className("brand");
	public By register = By.xpath("//p[@id='loginbar']/a[1]");
	public By login = By.xpath("//p[@id='loginbar']/a[2]");
	public By homeLink = By.xpath("//div[@class='container']/div/ul/li[1]/a");
	public By beersLink = By.xpath("//div[@class='container']/div/ul/li[2]/a");
	public By contactLink = By.xpath("//div[@class='container']/div/ul/li[3]/a");
	public By aboutLink = By.xpath("//div[@class='container']/div/ul/li[4]/a");
	
	
}
