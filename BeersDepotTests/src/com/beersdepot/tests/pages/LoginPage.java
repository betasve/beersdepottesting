package com.beersdepot.tests.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;
import com.beersdepot.tests.config.*;

public class LoginPage extends TestBase {

	public LoginPage (WebDriver driver) {
		if(!"Login".equals(driver.findElement(By.xpath("//div[@class='content']/h1")).getText())){
			throw new IllegalStateException("This is not the login page");
		}
	}
	
	public By pageTitle = By.xpath("//div[@class='content']/h1");
	public By username = By.xpath("//input[@id='email']");
	public By pass = By.xpath("//input[@id='password']");
	public By submitBtn = By.xpath("//input[@value='Login']");
	
	public void login (String user, String password){
		try{
			driver.findElement(username).sendKeys(user);
			driver.findElement(pass).sendKeys(password);
			driver.findElement(submitBtn).submit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
