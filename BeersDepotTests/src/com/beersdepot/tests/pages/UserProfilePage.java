package com.beersdepot.tests.pages;

import org.openqa.selenium.WebDriver;
import java.util.List;
import org.openqa.selenium.WebElement;
import com.beersdepot.tests.config.*;
import org.openqa.selenium.By;

public class UserProfilePage extends TestBase {
	
	public UserProfilePage (WebDriver driver){
		//this.driver = driver;
		
		if(!"Georgi Lalev".equals(driver.findElement(By.xpath("//div[@class='content']/div[1]/div[2]/h1")).getText())){
			throw new IllegalStateException("This is not the Georgi Lalev's profile page");
		}
	}
	// Left column tabs
	public By profilePicture = By.xpath("//div[@class='span4']/div/img");
	public By followingIcon = By.xpath("//div[@class='user_icons']/div/img");
	public By followingNum = By.xpath("//div[@class='user_icons']/div");
	public By openedBeers = By.xpath("//div[@class='user_icons']/div[2]/img");
	public By openedBeersNum = By.xpath("//div[@class='user_icons']/div[2]");
	public By addedBeers = By.xpath("//div[@class='user_icons']/div[3]/img");
	public By addedBeersNum = By.xpath("//div[@class='user_icons']/div[3]/text");
	public By favBeers = By.xpath("//div[@class='user_icons']/div[4]/img");
	public By favBeersNum = By.xpath("//div[@class='user_icons']/div[4]/text");
	public By followers = By.xpath("//div[@class='user_icons']/div[5]/img");
	public By followersNum = By.xpath("//div[@class='user_icons']/div[5]/text");
	public By profileInfo = By.xpath("//div[@id='info']/ul/li[2]");
	public By profileInfoFlag = By.xpath("//div[@id='info']/ul/li[2]/img");
	
	// Right column tabs
	public By beersFlowTab = By.xpath("//ul[@id='Tabs']/li[1]/a");
	public By statisticsTab = By.xpath("//ul[@id='Tabs']/li[2]/a");
	public By addedBeersTab = By.xpath("//ul[@id='Tabs']/li[3]/a");
	public By favBeersTab = By.xpath("//ul[@id='Tabs']/li[4]/a");
	
	public By beersFlowContent = By.xpath("//div[@id='activities']");
	public By statsContent = By.xpath("//div[@id='stat']");
	public By addedBeersContent = By.xpath("//div[@id='added']");
	public By favBeersContent = By.xpath("//div[@id='favourites']");
	
	
	public static List<WebElement> getTabRows (By element){
		List<WebElement> rows = null;
		try{
			rows = driver.findElements(element);
		} catch (Exception e){
			e.printStackTrace();
		}
		return rows;
	}
}
