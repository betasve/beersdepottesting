package com.beersdepot.tests.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;
import com.beersdepot.tests.config.*;


import java.util.List;

public class HomePage extends TestBase{

	//private final WebDriver driver;
	
	public HomePage (WebDriver driver){
		//this.driver = driver;
		
		if(!"BeersDepot.com".equals(driver.getTitle())){
			throw new IllegalStateException("This is not the homepage");
		}
	}
	
	// Get locators for the homepage
	
	public By siteLogo = By.className("brand");
	public By register = By.xpath("//p[@id='loginbar']/a[1]");
	public By login = By.xpath("//p[@id='loginbar']/a[2]");
	public By homeLink = By.xpath("//div[@class='container']/div/ul/li[1]/a");
	public By beersLink = By.xpath("//div[@class='container']/div/ul/li[2]/a");
	public By contactLink = By.xpath("//div[@class='container']/div/ul/li[3]/a");
	public By aboutLink = By.xpath("//div[@class='container']/div/ul/li[4]/a");
	public By header = By.xpath("//div[@class='container']/div/div/h1"); // [@class='hero-unit home-img']  [@class='row']
	public By subheader = By.xpath("//div[@class='container']/div/div/h2"); //[@class='hero-unit home-img'] [@class='row'] 
	public By searchBox = By.xpath("//form[@class='form-search']/input");
	public By searchButton = By.xpath("//form[@class='form-search']/button");
	public By addBeer = By.xpath("//a[@id='add_beer']");
	public By browseCountry = By.xpath("//a[@id='random-country']");
	public By randomBeer = By.xpath("//a[@id='random-beer']");
	
	public void search(String text){
		driver.findElement(searchBox).sendKeys(text);
		driver.findElement(searchButton).submit();
	}
}
