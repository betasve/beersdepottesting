package com.beersdepot.tests.pages;

import org.openqa.selenium.By;
import java.util.List;
import com.beersdepot.tests.config.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SearchPage extends TestBase {
	
	public SearchPage (WebDriver driver){
		if(!"Search Results".equals(driver.findElement(pageTitle).getText())){
			throw new IllegalStateException("This is not the search page");
		}
	}

	public By pageTitle = By.xpath("//div[@class='content']/div/div/h1");
	public By tabBeers = By.xpath("//a[@id='tbeers']");
	public By beers = By.xpath("//div[@id='beers']/table/tbody/tr");
	public By tabBreweries = By.xpath("//a[@id='tbreweries']");
	public By breweries = By.xpath("//div[@id='breweries']/table/tbody/tr");
	public By tabStyles = By.xpath("//a[@id='tstyles']");
	public By styles = By.xpath("//div[@id='styles']/table/tbody/tr");
	public By tabCategories = By.xpath("//a[@id='tcategories']");
	public By categories = By.xpath("//div[@id='categories']/table/tbody/tr");
	public By tabCountries = By.xpath("//a[@id='tcountries']");
	public By countries = By.xpath("//div[@id='countries']/table/tbody/tr");
	public By tabComments = By.xpath("//a[@id='tcomments']");
	public By comments = By.xpath("//div[@id='comments']/table/tbody/tr");
	public By tabUsers = By.xpath("//a[@id='tusers']");
	public By users = By.xpath("//div[@id='users']/table/tbody/tr");
	
	
	public static List<WebElement> getTabResults (By element){
		List<WebElement> rows = driver.findElements(element);
		
		return rows;
	}
}
