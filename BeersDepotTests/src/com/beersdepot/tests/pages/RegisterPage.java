package com.beersdepot.tests.pages;

import com.beersdepot.tests.config.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;

public class RegisterPage extends TestBase {

	public RegisterPage (WebDriver driver){
		if(!"Create User".equals(driver.findElement(By.xpath("//div[@class='content']/h1")).getText())){
			throw new IllegalStateException("This is not the Register page.");
		}
	}
	
	public By email = By.xpath("//input[@id='email']");
	public By password = By.xpath("//input[@id='password'");
	public By regBtn = By.xpath("//input[@value='Create User']");
	
	public void registerUser (String email, String pass) {
		driver.findElement(this.email).sendKeys(email);
		driver.findElement(this.password).sendKeys(pass);
		driver.findElement(this.regBtn).click();
	}
	
}
