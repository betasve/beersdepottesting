package com.beersdepot.tests;

import com.beersdepot.tests.config.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.Timeouts;
import org.openqa.selenium.By;
import com.beersdepot.tests.pages.LoginPage;
import com.thoughtworks.selenium.SeleneseTestBase;

public class B1_Test_User_Exists extends TestBase{

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		TestUtil.visit("/login");
		
		LoginPage login = new LoginPage(driver);
		
		login.login("svetoslav@metaideas.bg", "ASD!@#");
		
		TestUtil.assertTextPresent("Login", login.pageTitle);
		
		driver.close();
	}

}
