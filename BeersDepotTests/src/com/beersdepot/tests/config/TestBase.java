package com.beersdepot.tests.config;

import com.beersdepot.tests.config.*;
import java.io.IOException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


public class TestBase{

	/**
	 * @param args
	 */
	private static Config cfg = new Config("D:\\etc\\www\\beersdepottesting\\BeersDepotTests\\src\\com\\beersdepot\\tests\\config\\config.properties");
	public static String baseURL = cfg.ReadProperty("BaseURL");
	public static WebDriver driver = new FirefoxDriver();
	
	
	public void initialize(){
		
	}
	
}
