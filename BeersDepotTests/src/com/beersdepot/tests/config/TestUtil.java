package com.beersdepot.tests.config;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;
import com.thoughtworks.selenium.SeleneseTestBase;
import java.util.List;


public class TestUtil extends TestBase {

	public static void visit(String uri) {
		try {
			StringBuilder url = new StringBuilder();
			url.append(baseURL);
			url.append(uri);
			driver.manage().window().maximize();
			driver.get(url.toString());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void assertTextPresent(String assText, By element){
		try{
			String text1 = driver.findElement(element).getText();
			SeleneseTestBase.assertEquals(assText, text1);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void click(By element) {
		try {
			driver.findElement(element).click();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static Boolean findRow (String name, List<WebElement> rows){
		Boolean elementFound = false;
		try{
			String tdText;
			int rowCount = rows.size();
			
			for (int i = 0; i < rowCount; i++){
				tdText = rows.get(i).findElement(By.xpath("td/a")).getText();
				System.out.println(tdText + " with " + i + " and rowCount = " + rowCount + "\n");
				if(name.toString().equals(tdText.toString())){
					elementFound = true;
				}
			}
		} catch (Exception e){
			e.printStackTrace();
		}
		return elementFound;
	}
}
